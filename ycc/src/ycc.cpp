#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

using namespace std;


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

int main(int argc, char** argv)
{

  ros::init (argc, argv, "pub_pcl");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<PointCloud> ("points2", 1);

  PointCloud::Ptr msg (new PointCloud);
  msg->header.frame_id = "base_link";
  msg->height = 1;

  typedef vector<vector<float> > Rows;
  Rows rows;
  ifstream input("input.txt");
  char const row_delim = '\n';
  char const field_delim = '\t';
  float p;
  int i=0;
  for (string row; getline(input, row, row_delim); ) {
    rows.push_back(Rows::value_type());
    istringstream ss(row);
    for (string field; getline(ss, field, field_delim); ) {
      rows.back().push_back(stof(field));
      //cout << field << " ";
    }
    //cout << endl;

    if(i == 0){
       p = rows[0][0];
    }
    else if(i == 1){
       msg->width = rows[1][0];
    }
    else if(rows[i].size() == 3){
      msg->points.push_back (pcl::PointXYZ(rows[i][0], rows[i][1], rows[i][2] ));
    }


    i++;
  }

  ros::Rate loop_rate(4);
  while (nh.ok())
  {
    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
    pub.publish (msg);
    ros::spinOnce ();
    loop_rate.sleep ();
  }
}

