#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import std_msgs
from sensor_msgs.msg import PointCloud2
import csv

def talker():
    # Функция init_node() инициализирует подсистему ROS.
    # Тут задается имя ноды. Параметр anonymous=True указывает, что при запуске будет выбранно
    # уникадбное имя. Анонимные ноды могут быть запущены одновременно, не мешая друг другу.
    rospy.init_node('talker', anonymous=True)

    p = 0
    with open('input.txt', "rb" ) as input_file:
      input_reader = csv.reader(input_file, delimiter='\t')
      msg = PointCloud2()
      msg.header = std_msgs.msg.Header()
      msg.header.stamp = rospy.Time.now()
      msg.height = 1
      p = input_reader.pop(0)
      msg.width = input_reader.pop(0)
      for data in input_reader:
      #  print(data)

    # При помощи функции rospy.Publisher можно сказать ROS, что мы хотим опубликовать
    # сообщения в топик с определенным именем. Первым параметром передается имя топика,
    # вторым тип получаемых сообщений, третьим параметром передаются различные опции.
    # Функция возвращает объект Publisher, который позволяет публиковать сообщения в топик,
    # вызвая функцию publish(). Топик автоматически закроется как только все копии объекта
    # Publisher будут уничтожены.
    pub = rospy.Publisher('chatter', PointCloud2,  queue_size=10)
    rate = rospy.Rate(10) # 10hz
    i = 0
    while not rospy.is_shutdown():
        # создаём и заполняем объект Hello

        #msg.data = "hello world!"
        rospy.loginfo("%s", msg.data)
        # публикуем сообщение
        pub.publish(msg)
        rate.sleep()
        i += 1

def main():
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
